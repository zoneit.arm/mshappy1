from django.apps import AppConfig


class ChartaiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'chartai'
