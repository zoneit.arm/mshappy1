from django.db import models

# Create your models here.
class datachart(models.Model):
    teamvs = models.CharField(max_length=255)
    selecttime = models.CharField(max_length=255)
    formula = models.CharField(max_length=255)
    score = models.CharField(max_length=10)
    scoreafter = models.CharField(max_length=10,default='')
    range = models.CharField(max_length=20)
    price = models.CharField(max_length=20)
    status = models.CharField(max_length=20,default='')
    package = models.CharField(max_length=20,default='')
    datetimes = models.DateTimeField(null=True,blank=True,auto_now_add=True)
    select_price = models.IntegerField()
