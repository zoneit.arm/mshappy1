from django.shortcuts import render,redirect
from django.http import HttpResponse,JsonResponse
from django.template import loader
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt

from chartai import models
import math

from django.db.models import Count
# Create your views here.
@csrf_protect
@csrf_exempt
def chartai(request):
    template = loader.get_template('chartai.html')

    datas1 = models.datachart.objects.filter(status="win").values('formula').annotate(total=Count('formula'))
    sss = RequestContext(request)
    label1 = []
    data1 = []
    data2 = []
    for aa in datas1:
        datas = models.datachart.objects.filter(formula=str(aa["formula"])).count()
        label1.append(aa["formula"])
        data1.append(aa["total"])
        data2.append((int(aa["total"])/int(datas))*100)
    response = {
        "label1":label1,
        "label2":label1,
        "data1":data1,
        "data2":data2
    }
    return HttpResponse(template.render(response,request))

def getchart(request):
    datas = models.datachart.objects.all().values()
    data = list(datas)
    return JsonResponse(data,safe=False)

def getdatatable(request):
    if request.method == "GET":
        datas = models.datachart.objects.all().values()
        total = datas.count()
        _start = request.GET.get('start')
        _length = request.GET.get('length')
        page = 0
        per_page = 0
        if _start and _length:
            start = int(_start)
            length = int(_length)
            page = math.ceil(start / length) + 1
            per_page = length

            datas = datas[start:start + length]
        data = list(datas)
        response = {
            "data":data,
            "page": page,
            'per_page': per_page,  # [opcional]
            'recordsTotal': total,
            'recordsFiltered': total,
        }
        return JsonResponse(response)
    return JsonResponse({'message': 'error'})