from django.urls import path
from . import views

urlpatterns = [
    path('', views.chartai, name='chartai'),
    path('getdatatable1/',views.getdatatable,name="getdatatable1"),
    path('getchart/',views.getchart,name="getchart")
]