# Generated by Django 4.2 on 2023-04-14 18:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='specs',
            name='created_at',
            field=models.TimeField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='specs',
            name='role',
            field=models.CharField(default='member', max_length=20),
        ),
    ]
