# Generated by Django 4.2 on 2023-04-14 18:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0002_specs_created_at_specs_role'),
    ]

    operations = [
        migrations.AddField(
            model_name='specs',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AlterField(
            model_name='specs',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
