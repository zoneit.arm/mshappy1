from django.urls import path
from . import views

urlpatterns = [
    path('', views.login, name='login'),
    path('logins/', views.logins),
    path('register/', views.register, name='register'),
    path('registers/', views.registers,),
]